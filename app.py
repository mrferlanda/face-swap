import os
from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename
from flask import send_from_directory
from photos_face_swapping import swap_image
import datetime
cur_time = datetime.datetime.now().strftime("%Y%m%d%H%M%S")

UPLOAD_FOLDER = 'static'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files or 'file2' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        file2 = request.files['file2']

        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file1')
            return redirect(request.url)
        if file2.filename == '':
            flash('No selected file2')
            return redirect(request.url)
        if (file and allowed_file(file.filename)) and (file2 and allowed_file(file2.filename)):
            filename1 = secure_filename(file.filename)
            filename2 = secure_filename(file2.filename)
            file1_path = os.path.join(app.config['UPLOAD_FOLDER'], cur_time + '-'+ filename1)
            file2_path = os.path.join(app.config['UPLOAD_FOLDER'], cur_time + '-'+ filename2)
            file.save(file1_path)
            file2.save(file2_path)
            return swap_image(filename1, filename2,cur_time)
    return '''

    
    <!doctype html>
    <title>Face Swap App</title>
    <h1>Face Swap App</h1>
    <form method=post enctype=multipart/form-data>
      Source Image: <br>
      <input type=file name=file><br>
      Destination Image: <br>
      <input type=file name=file2><br><br>
      <input type=submit value=SWAP!!><br>
    </form>
    '''

if  __name__ == '__main__':
    app.run(host="0.0.0.0",debug=True)