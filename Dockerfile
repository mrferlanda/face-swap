# syntax=docker/dockerfile:1
FROM python:3.8-slim-buster
WORKDIR /app
RUN apt-get update -y && \
    apt-get install -y python-pip python-dev ffmpeg libsm6 libxext6 cmake
COPY . /app
RUN pip install -r requirements.txt
EXPOSE 5000
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]