- [How to Use](#how-to-use)
  - [Local Environment](#local-environment)
  - [Docker Container](#docker-container)
  - [Docker Swarm (Experimental)](#docker-swarm-experimental)
- [Web Interface](#web-interface)
- [Example Input & Output](#example-input--output)


# How to Use 
## Local Environment

First, make sure you install all of the required dependencies

``
pip install -r requirements.txt
``

After that, just run the flask

``
flask run
``

By default, you can access it through http://127.0.0.1:5000

## Docker Container

Pull the image from docker hub

``
docker pull mrferlanda/faceswap:latest
``

and simply just run

``
docker run -p 5000:5000 mrferlanda/faceswap
``

## Docker Swarm (Experimental)

- Updated Soon

# Web Interface

If you've followed the instruction carefully, you will be able to see this page.
![interface](interface.png)

Source Image: upload the face image you want to extract.

Destination Image: upload the image of the face you want to transform using the source face image.

Press `SWAP!!` button to start the process.

# Example Input & Output

![io](io.jpg)

Note: **This application is developed while doing an internship at Nodeflux on 2022**

[Source](https://pysource.com/2019/05/28/face-swapping-explained-in-8-steps-opencv-with-python/)


